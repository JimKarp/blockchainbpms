Prerequisites:
• Node.js
• NPM
• Angular 10
• Truffle
• Solidity Compiler

Version of Truffle and Solidity uses this app:
• Truffle v5.0.5 (core: 5.0.5)
• Solidity v0.5.16 (solc-js)

The steps to get started:

1) Install Ganache and Truffle for your OS using below commands. Ganache is a Personal blockchain for Ethereum development. Truffle is a framework for blockchains using the Ethereum.

   `> npm install -g ganache-cli`
   
   `> npm install -g truffle@5.0.5`

2) Run the blockchain network.
  
   2.1) Run Ganache using below command.
   
   `> ganache-cli`
   
   OR
   
   2.2) Open and run the private Ethereum blockchain.

3) Compile the truffle contracts and deploy to network.

   3.1) If you are using ganache, execute the following command.
   
   `> truffle migrate --reset --network ganache --compile-all`
   
   OR
   
   3.2) If you are using private Ethereum network, execute the following command.
   
   `> truffle migrate --reset --network rpc --compile-all`

4) Navigate to the Angular application folder, install dependencies and run. Then navigate with your browser to `http://localhost:4200/`

   `> cd blockchain-bpms`

   `> npm install`
   
   `> ng serve --open`