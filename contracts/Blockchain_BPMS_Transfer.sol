pragma solidity 0.5.16;

contract Blockchain_BPMS_Transfer {

    address payable from;
    address payable to;
  
    constructor() public {
        from = msg.sender;
    }
  
    event TransferFund(address _to, address _from, uint amount);
    
    function transferFund( address payable _to ) public payable returns (bool) {
        to = _to;
        to.transfer(msg.value);
        emit TransferFund(to, from, msg.value);
        return true;
    }

}
