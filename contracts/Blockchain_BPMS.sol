pragma solidity 0.5.16;

contract Blockchain_BPMS {
    
    struct Employee{
        address employeeAddress;
        string name;
        string surname;
        string department;
        int tasksCompleted;
        int tasksPending;
        uint [] contributions;
    }
    
    struct Supervisor{
        address supervisorAddress;
        string name;
        string surname;
        string department;
        int processesApproved;
        int processesRejected;
    }
    
    struct Director{
        address directorAddress;
        string name;
        string surname;
        string department;
    }
    
    struct User{
        address userAddress;
        string userRole;
        uint id;
    }
    
    struct Task{
        string taskName;
        uint generatedBy;
        uint timestampGenerated;
        uint acceptedBy;
        uint timestampAccepted;
        uint completedBy;
        uint timestampCompleted;
        string status;
    }

    struct Process{
        string processName;
        uint generatedBy;
        uint timestampGenerated;
        uint endedBy;
        uint timestampEnded;
        string status;
        uint checkedBy;
        uint timestampChecked;
        int tasksCounter;
        mapping (int => Task) tasks;
        string document;
    }
    
    mapping (uint => Employee) public employees;
    uint public employeeId;

    mapping (uint => Supervisor) public supervisors;
    uint public supervisorId;
    
    mapping (uint => Director) public directors;
    uint public directorId;
    
    mapping (address => User) public userList;

    mapping (uint => Process) public processes;
    uint public processId;

    constructor () public {
        User storage user = userList[0x7103C7508222d5503f408b40171aB8cC927a2668];
        user.userAddress = 0x7103C7508222d5503f408b40171aB8cC927a2668;
        user.userRole = "Admin";
    }

    // based on https://ethereum.stackexchange.com/questions/3285/how-to-get-return-values-when-function-with-argument-is-called
    event newEmployee(uint employeeId);

    function addEmployee(address _employeeAddress, string memory _name, string memory _surname, string memory _department) public returns(bool){
        address a;
        (a, , ) =  getUser(_employeeAddress);
        if(a==address(0)){
            employeeId++;
            Employee storage employee = employees[employeeId];
            employee.employeeAddress = _employeeAddress;
            employee.name = _name;
            employee.surname = _surname;
            employee.department = _department;
            employee.tasksCompleted = 0;
            employee.tasksPending = 0;
            User storage user = userList[_employeeAddress];
            user.userAddress = _employeeAddress;
            user.userRole = "Employee";
            user.id = employeeId;
            emit newEmployee(employeeId);
            return true;
        } else {
            emit newEmployee(0);
            return false;
        }
    }

    event newSupervisor(uint supervisorId);
    
    function addSupervisor(address _supervisorAddress, string memory _name, string memory _surname, string memory _department) public returns(bool){
        address a;
        (a, , ) =  getUser(_supervisorAddress);
        if(a==address(0)){
            supervisorId++;
            Supervisor storage supervisor = supervisors[supervisorId];
            supervisor.supervisorAddress = _supervisorAddress;
            supervisor.name = _name;
            supervisor.surname = _surname;
            supervisor.department = _department;
            supervisor.processesApproved = 0;
            supervisor.processesRejected = 0;
            User storage user = userList[_supervisorAddress];
            user.userAddress = _supervisorAddress;
            user.userRole = "Supervisor";
            user.id = supervisorId;
            emit newSupervisor(supervisorId);
            return true;
        } else {
            emit newSupervisor(0);
            return false;
        }
    }

    event newDirector(uint directorId);
    
    function addDirector(address _directorAddress, string memory _name, string memory _surname, string memory _department) public returns(bool){
        address a;
        (a, , ) =  getUser(_directorAddress);
        if(a==address(0)){
            directorId++;
            Director storage director = directors[directorId];
            director.directorAddress = _directorAddress;
            director.name = _name;
            director.surname = _surname;
            director.department = _department;
            User storage user = userList[_directorAddress];
            user.userAddress = _directorAddress;
            user.userRole = "Director";
            user.id = directorId;
            emit newDirector(directorId);
            return true;
        } else {
            emit newDirector(0);
            return false;
        }
    }

    function editEmployee(uint _employeeId, string memory _department) public returns(bool){
        employees[_employeeId].department = _department;
        return true;
    }

    function editSupervisor(uint _supervisorId, string memory _department) public returns(bool){
        supervisors[_supervisorId].department = _department;
        return true;
    }

    function editDirector(uint _directorId, string memory _department) public returns(bool){
        directors[_directorId].department = _department;
        return true;
    }

    event newProcess(uint processId);

    function generateProcess(string memory _processName, uint _employeeId, string memory _firstTaskName) public returns(bool){
        processId++;
        Process storage process = processes[processId];
        process.processName = _processName;
        process.generatedBy = _employeeId;
        process.timestampGenerated = now;
        process.status = "Pending";
        process.tasksCounter++;
        process.tasks[process.tasksCounter].taskName = _firstTaskName;
        process.tasks[process.tasksCounter].generatedBy = _employeeId;
        process.tasks[process.tasksCounter].timestampGenerated = now;
        process.tasks[process.tasksCounter].acceptedBy = _employeeId;
        process.tasks[process.tasksCounter].timestampAccepted = now;
        process.tasks[process.tasksCounter].status = "Pending";
        Employee storage employee = employees[_employeeId];
        employee.tasksPending++;
        emit newProcess(processId);
        return true;
    }

    function setDocument(string memory _document, uint _processId, uint _employeeId) public returns(bool){
        Process storage process = processes[_processId];
        if(process.tasks[process.tasksCounter].acceptedBy == _employeeId &&
        keccak256(abi.encodePacked(process.tasks[process.tasksCounter].status)) == keccak256(abi.encodePacked("Pending"))
        ){
            process.document = _document;
            return true;
        }
        return false;
    }
    
    function addTask(uint _processId, string memory _taskName, uint _employeeId, bool _accept) public returns(bool){
        Process storage process = processes[_processId];
        if(keccak256(abi.encodePacked(process.status)) == keccak256(abi.encodePacked("Pending"))){
            if(keccak256(abi.encodePacked(process.tasks[process.tasksCounter].status)) == keccak256(abi.encodePacked("Completed"))){
                process.tasksCounter++;
                process.tasks[process.tasksCounter].taskName = _taskName;
                process.tasks[process.tasksCounter].generatedBy = _employeeId;
                process.tasks[process.tasksCounter].timestampGenerated = now;
                if (_accept) {
                    Employee storage employee = employees[_employeeId];
                    process.tasks[process.tasksCounter].status = "Pending";
                    process.tasks[process.tasksCounter].acceptedBy = _employeeId;
                    process.tasks[process.tasksCounter].timestampAccepted = now;
                    employee.tasksPending++;
                } else {
                    process.tasks[process.tasksCounter].status = "Open";
                }
                return true;
            }
        }
        return false;
    }
    
    function acceptTask(uint _processId, int _taskId, uint _employeeId) public returns(bool){
        Process storage process = processes[_processId];
        if(keccak256(abi.encodePacked(process.status)) == keccak256(abi.encodePacked("Pending"))){
            if(process.tasks[_taskId].acceptedBy == 0 && keccak256(abi.encodePacked(process.tasks[_taskId].status)) == keccak256(abi.encodePacked("Open"))){
                Employee storage employee = employees[_employeeId];
                process.tasks[_taskId].acceptedBy = _employeeId;
                process.tasks[_taskId].timestampAccepted = now;
                process.tasks[_taskId].status = "Pending";
                employee.tasksPending++;
                return true;
            }
        }
        return false;
    }
    
    function completeTask(uint _processId, int _taskId, uint _employeeId) public returns(bool){
        Process storage process = processes[_processId];
        if(keccak256(abi.encodePacked(process.status)) == keccak256(abi.encodePacked("Pending"))){
            if(process.tasks[_taskId].acceptedBy == _employeeId && keccak256(abi.encodePacked(process.tasks[_taskId].status)) == keccak256(abi.encodePacked("Pending"))){
                Employee storage employee = employees[_employeeId];
                process.tasks[_taskId].completedBy = _employeeId;
                process.tasks[_taskId].timestampCompleted = now;
                process.tasks[_taskId].status = "Completed";
                employee.tasksPending--;
                employee.tasksCompleted++;
                employee.contributions.push(now);
                return true;
            }
        }
        return false;
    }
    
    function completeProcess(uint _processId, uint _employeeId) public returns(bool){
        Process storage process = processes[_processId];
        if(keccak256(abi.encodePacked(process.status)) == keccak256(abi.encodePacked("Pending"))){
            if(keccak256(abi.encodePacked(process.tasks[process.tasksCounter].status)) == keccak256(abi.encodePacked("Completed"))){
                process.endedBy = _employeeId;
                process.timestampEnded = now;
                process.status = "Completed";
                return true;
            }
        }
        return false;
    }

    function cancelProcess(uint _processId, uint _employeeId) public returns(bool){
        Process storage process = processes[_processId];
        if(keccak256(abi.encodePacked(process.status)) == keccak256(abi.encodePacked("Pending"))){
            process.endedBy = _employeeId;
            process.timestampEnded = now;
            process.status = "Canceled";
            if(keccak256(abi.encodePacked(process.tasks[process.tasksCounter].status)) == keccak256(abi.encodePacked("Pending"))){
                Employee storage employee = employees[process.tasks[process.tasksCounter].acceptedBy];
                process.tasks[process.tasksCounter].status = "Canceled";
                employee.tasksPending--;
            }else if(keccak256(abi.encodePacked(process.tasks[process.tasksCounter].status)) == keccak256(abi.encodePacked("Open"))){
                process.tasks[process.tasksCounter].status = "Canceled";
            }
            return true;
        }
        return false;
    }

    function approveProcess(uint _processId, uint _supervisorId) public returns(bool){
        Process storage process = processes[_processId];
        if(keccak256(abi.encodePacked(process.status)) == keccak256(abi.encodePacked("Completed"))){
            process.checkedBy = _supervisorId;
            process.timestampChecked = now;
            process.status = "Approved";
            Supervisor storage supervisor = supervisors[_supervisorId];
            supervisor.processesApproved++;
            return true;
        }
        return false;
    }

    function rejectProcess(uint _processId, uint _supervisorId) public returns(bool){
        Process storage process = processes[_processId];
        if(keccak256(abi.encodePacked(process.status)) == keccak256(abi.encodePacked("Completed"))){
            process.checkedBy = _supervisorId;
            process.timestampChecked = now;
            process.status = "Rejected";
            Supervisor storage supervisor = supervisors[_supervisorId];
            supervisor.processesRejected++;
            return true;
        }
        return false;
    }

    function getTasksCounter(uint _processId) view public returns(int){
        Process memory process = processes[_processId];
        return process.tasksCounter;
    }

    function getEmployee(uint _employeeId) view public returns(uint, address, string memory, string memory, string memory, int, int, uint [] memory){
        Employee memory employee = employees[_employeeId];
        return(_employeeId, employee.employeeAddress, employee.name, employee.surname, employee.department, employee.tasksCompleted, employee.tasksPending, employee.contributions);
    }
    
    function getSupervisor(uint _supervisorId) view public returns(uint, address, string memory, string memory, string memory, int, int){
        Supervisor memory supervisor = supervisors[_supervisorId];
        return(_supervisorId, supervisor.supervisorAddress, supervisor.name, supervisor.surname, supervisor.department, supervisor.processesApproved, supervisor.processesRejected);
    }
    
    function getDirector(uint _directorId) view public returns(uint, address, string memory, string memory, string memory){
        Director memory director = directors[_directorId];
        return(_directorId, director.directorAddress, director.name, director.surname, director.department);
    }
    
    function getUser(address _userAddress) view public returns(address, string memory, uint){
        User memory user = userList[_userAddress];
        return(user.userAddress, user.userRole, user.id);
    }
    
    function getProcess(uint _processId) view public returns(uint, string memory, uint, uint, uint, uint, uint, uint, string memory, int, string memory){
        Process memory process = processes[_processId];
        return(_processId, process.processName, process.generatedBy, process.timestampGenerated, process.endedBy, process.timestampEnded, process.checkedBy, process.timestampChecked, process.status, process.tasksCounter, process.document);
    }
    
    function getTask(uint _processId, int _taskId) view public returns(int, string memory, uint, uint, uint, uint, uint, uint, string memory){
        Process storage process = processes[_processId];
        Task memory task = process.tasks[_taskId];
        return(_taskId, task.taskName, task.generatedBy, task.timestampGenerated, task.acceptedBy, task.timestampAccepted, task.completedBy, task.timestampCompleted, task.status);
    }
    
}
