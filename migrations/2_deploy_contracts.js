const Blockchain_BPMS = artifacts.require("./Blockchain_BPMS.sol");
const Blockchain_BPMS_Transfer = artifacts.require("./Blockchain_BPMS_Transfer.sol");

module.exports = function(deployer) {
  deployer.deploy(Blockchain_BPMS);
  deployer.deploy(Blockchain_BPMS_Transfer);
};
