import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-calendar-chart',
  templateUrl: './calendar-chart.component.html'
})
export class CalendarChartComponent implements OnInit {
  @Input() contributions;
  title = 'Contributions';
  type = 'Calendar';
  data = [];
  options = {
    width: 950,
    height: 180,
    colorAxis: {
      colors: ['#b3f6ff', '#0b3473']
    }
  };
  contribution_dates = [];
  temp_data = [];
  current = null;
  first_year = 0;
  last_year = 0;
  total_years = 0;

  constructor() { }

  ngOnInit() {
    this.contributions.sort();
    for(let contribution of this.contributions) {
      let date = new Date(contribution * 1000);
      date.setHours(0,0,0,0);
      this.contribution_dates.push(date);
    }

    if(this.contribution_dates.length){
      this.first_year = this.contribution_dates[0].getFullYear();
      this.last_year = this.contribution_dates[this.contribution_dates.length - 1].getFullYear();
      this.total_years = (this.last_year - this.first_year) + 1;
      this.options.height = this.total_years * 145 + 50;
      for (let i = 0; i < this.contribution_dates.length; i++) {
        if (!this.current || this.contribution_dates[i].getTime() != this.current.getTime()) {
          this.temp_data.push([this.contribution_dates[i], 1]);
        } else {
          this.temp_data[this.temp_data.length-1][1]++;
        }
        this.current = this.contribution_dates[i];
      }

      this.data = this.temp_data;
    }
    
  }

}