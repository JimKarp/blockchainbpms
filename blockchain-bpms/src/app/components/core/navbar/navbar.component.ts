import { Component, OnInit, Input } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { EthcontractService } from 'src/app/services/ethcontract.service';
import { User } from 'src/app/models/user';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html'
})
export class NavbarComponent implements OnInit {
  navbarItems: any;
  currentUser: User;

  constructor(
    private dataService: DataService,
    private ethcontractService: EthcontractService
  ) {}

  ngOnInit() {
    this.ethcontractService.currentUser.subscribe(currentUser => {
      this.currentUser = currentUser;
      if(currentUser) {
        this.dataService.getNavbarItems(currentUser.role).subscribe(navbarItems => this.navbarItems = navbarItems);
      }
    });
  }
}
