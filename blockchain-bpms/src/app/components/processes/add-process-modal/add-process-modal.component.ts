import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { EthcontractService } from 'src/app/services/ethcontract.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { User } from 'src/app/models/user';
import { Role } from 'src/app/models/role';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-process-modal',
  templateUrl: './add-process-modal.component.html'
})
export class AddProcessModalComponent implements OnInit, OnDestroy {
  @Input() modalId;
  processform: FormGroup;
  currentUser: User;
  private subscription: Subscription;
  isSaving = false;

  constructor(
    private snackBar: MatSnackBar,
    public activeModal: NgbActiveModal,
    private ethcontractService: EthcontractService,
    private router: Router,
    private fb: FormBuilder
  ) {
    this.subscription = this.ethcontractService.currentUser.subscribe(currentUser => this.currentUser = currentUser);
  }

  ngOnInit() {
    this.processform = this.fb.group({
      processName: ['', Validators.required],
      taskName: ['', Validators.required]
    });
  }

  get isEmployee() {
    return this.currentUser && this.currentUser.role === Role.Employee;
  }

  submit() {
    if (this.processform.valid) {
      this.isSaving = true;
      this.ethcontractService.generateProcess(this.processform.value.processName, this.processform.value.taskName).then(result => {
        if(result === -1){
          this.snackBar.open('Please confirm transaction in order to generate the process', '', {
            duration: 10000
          });
        }else if(result){
          this.snackBar.open('Process generated successfully', '', {
            duration: 3000
          });
          this.activeModal.dismiss();
          this.router.navigate(['/processes', result]);
        } else {
          this.snackBar.open('Error generating process', '', {
            duration: 3000
          });
        }
        this.isSaving = false;
      });
    }
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}