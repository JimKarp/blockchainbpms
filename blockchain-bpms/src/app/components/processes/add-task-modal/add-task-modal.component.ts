import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { EthcontractService } from 'src/app/services/ethcontract.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { User } from 'src/app/models/user';
import { Role } from 'src/app/models/role';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-add-task-modal',
  templateUrl: './add-task-modal.component.html'
})
export class AddTaskModalComponent implements OnInit, OnDestroy {
  @Input() processId;
  taskform: FormGroup;
  currentUser: User;
  private subscription: Subscription;
  isSaving = false;

  constructor(
    private snackBar: MatSnackBar,
    public activeModal: NgbActiveModal,
    private ethcontractService: EthcontractService,
    private fb: FormBuilder
  ) {
    this.subscription = this.ethcontractService.currentUser.subscribe(currentUser => this.currentUser = currentUser);
  }

  ngOnInit() {
    this.taskform = this.fb.group({
      taskName: ['', Validators.required],
      accept: [false, Validators.required]
    });
  }

  get isEmployee() {
    return this.currentUser && this.currentUser.role === Role.Employee;
  }

  submit() {
    if (this.taskform.valid) {
      this.isSaving = true;
      this.ethcontractService.addTask(this.processId, this.taskform.value.taskName, this.taskform.value.accept).then(result => {
        if(result){
          this.snackBar.open('Task added successfully', '', {
            duration: 3000
          });
          this.activeModal.close(true);
        }else{
          this.snackBar.open('Please confirm transaction in order to add the task', '', {
            duration: 10000
          });
        }
        this.isSaving = false;
      });
    }
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}