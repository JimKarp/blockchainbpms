import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { EthcontractService } from '../../services/ethcontract.service';
import { User } from 'src/app/models/user';
import { Subscription } from 'rxjs';
import { Task } from 'src/app/models/task';

@Component({
  selector: 'app-processes',
  templateUrl: './processes.component.html'
})
export class ProcessesComponent implements OnInit, OnDestroy {
  currentUser: User;
  openTasks: Task[];
  rotate: boolean = false;
  private subscription: Subscription;
  private subscription2: Subscription;
  
  constructor(
    private ethcontractService: EthcontractService
  ) {
    this.subscription = this.ethcontractService.currentUser.subscribe(currentUser => this.currentUser = currentUser);
  }

  ngOnInit() {
    this.subscription2 = this.ethcontractService.getOpenTasks().subscribe(openTasks => this.openTasks = openTasks);
  }

  reloadOpenTasks() {
    this.rotate = true;
    this.subscription2 = this.ethcontractService.getOpenTasks().subscribe(openTasks => {
      this.openTasks = openTasks
      setTimeout(() => {
        this.rotate = false;
      }, 1000);
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
    this.subscription2.unsubscribe();
  }

}
