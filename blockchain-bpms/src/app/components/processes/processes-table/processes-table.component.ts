import { Component, OnInit, OnDestroy } from '@angular/core';
import { EthcontractService } from 'src/app/services/ethcontract.service';
import { AddProcessModalComponent } from '../add-process-modal/add-process-modal.component';
import { Process } from 'src/app/models/process';
import { User } from 'src/app/models/user';
import { Subscription } from 'rxjs';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Role } from 'src/app/models/role';

@Component({
  selector: 'app-processes-table',
  templateUrl: './processes-table.component.html'
})
export class ProcessesTableComponent implements OnInit, OnDestroy {
  currentUser: User;
  processes: Process[] = [];
  isAsc: string;
  isDesc: string;
  isClosed: boolean;
  rotate: boolean = false;
  searchValue: string;
  private subscription: Subscription;
  private subscription2: Subscription;

  constructor(
    private ethcontractService: EthcontractService,
    private modalService: NgbModal
  ) {
    this.subscription = this.ethcontractService.currentUser.subscribe(currentUser => this.currentUser = currentUser);
  }

  ngOnInit() {
    this.subscription2 = this.ethcontractService.getProcesses().subscribe(processes => this.processes = processes);
  }

  reloadProcesses() {
    this.rotate = true;
    this.subscription2 = this.ethcontractService.getProcesses().subscribe(processes => {
      this.processes = processes.sort((a, b) => a['processId'] < b['processId'] ? 1 : a['processId'] > b['processId'] ? -1 : 0);
      setTimeout(() => {
        this.rotate = false;
      }, 1000);
    });
  }

  // based on https://stackoverflow.com/questions/58820715/how-can-i-sort-html-table-using-angular-typescript-without-3rd-party
  sortTable(colName: string) {
    if (this.isAsc === colName) {
      this.isDesc = colName;
      this.isAsc = undefined;
      this.processes.sort((a, b) => a[colName] < b[colName] ? 1 : a[colName] > b[colName] ? -1 : 0);
    } else {
      this.isAsc = colName;
      this.isDesc = undefined;
      this.processes.sort((a, b) => a[colName] > b[colName] ? 1 : a[colName] < b[colName] ? -1 : 0);
    }
  }

  get isEmployee() {
    return this.currentUser && this.currentUser.role === Role.Employee;
  }

  openGenerateProcessModal() {
    const modalRef = this.modalService.open(AddProcessModalComponent);
    modalRef.componentInstance.modalId = 1;
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
    this.subscription2.unsubscribe();
  }

}
