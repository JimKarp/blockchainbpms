import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { EthcontractService } from 'src/app/services/ethcontract.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Task } from 'src/app/models/task';
import { User } from 'src/app/models/user';
import { Role } from 'src/app/models/role';
import { Subscription } from 'rxjs';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmationModalComponent } from '../../core/confirmation-modal/confirmation-modal.component';

@Component({
  selector: 'app-task-details-modal',
  templateUrl: './task-details-modal.component.html'
})
export class TaskDetailsModalComponent implements OnInit, OnDestroy {
  @Input() task: Task;
  currentUser: User;
  private subscription: Subscription;
  isSaving = false;

  constructor(
    private snackBar: MatSnackBar,
    public activeModal: NgbActiveModal,
    private ethcontractService: EthcontractService,
    private modalService: NgbModal
  ) {
    this.subscription = this.ethcontractService.currentUser.subscribe(currentUser => this.currentUser = currentUser);
  }

  ngOnInit() {}

  get isEmployee() {
    return this.currentUser && this.currentUser.role === Role.Employee;
  }

  get hasReadEmployeesPerm() {
    return this.currentUser && (this.currentUser.role === Role.Supervisor || this.currentUser.role === Role.Director || this.currentUser.role === Role.Admin);
  }

  acceptTask() {
    this.activeModal.close(false);
    const modalRef = this.modalService.open(ConfirmationModalComponent);
    modalRef.componentInstance.message = 'Are you sure that you want to assign yourself the task?';
    modalRef.result.then(confirm => {
      if(confirm){
        this.isSaving = true;
        console.log(this.task.processId)
        this.ethcontractService.acceptTask(this.task.processId, this.task.taskId).then(result => {
          if(!result){
            this.snackBar.open('Please confirm transaction in order to accept the task', '', {
              duration: 10000
            });
          }
          this.isSaving = false;
        });
      }
    });
  }

  completeTask() { 
    this.activeModal.close(false);
    const modalRef = this.modalService.open(ConfirmationModalComponent);
    modalRef.componentInstance.message = 'Are you sure that you want to complete the task?';
    modalRef.result.then(confirm => {
      if(confirm){
        this.isSaving = true;
        this.ethcontractService.completeTask(this.task.processId, this.task.taskId).then(result => {
          if(!result){
            this.snackBar.open('Please confirm transaction in order to complete the task', '', {
              duration: 10000
            });
          }
          this.isSaving = false;
        });
      }
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}