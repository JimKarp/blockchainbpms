import { Component, OnInit, OnDestroy } from '@angular/core';
import { EthcontractService } from 'src/app/services/ethcontract.service';
import { PdfService } from 'src/app/services/pdf.service';
import { ActivatedRoute } from '@angular/router';
import { Process } from 'src/app/models/process';
import { Task } from 'src/app/models/task';
import { User } from 'src/app/models/user';
import { Role } from 'src/app/models/role';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Subscription } from 'rxjs';
import { AddTaskModalComponent } from '../add-task-modal/add-task-modal.component';
import { ConfirmationModalComponent } from '../../core/confirmation-modal/confirmation-modal.component';
import { TaskDetailsModalComponent } from '../task-details-modal/task-details-modal.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-process-details',
  templateUrl: './process-details.component.html'
})
export class ProcessDetailsComponent implements OnInit, OnDestroy {
  process: Process;
  tasks: Task[];
  id: number;
  currentUser: User;
  showPlusActions: boolean = false;
  isSaving = false;
  validFields;
  private subscription1: Subscription;
  private subscription2: Subscription;
  private subscription3: Subscription;

  constructor(
    private snackBar: MatSnackBar,
    private ethcontractService: EthcontractService,
    private pdfService: PdfService,
    private route: ActivatedRoute,
    private modalService: NgbModal
  ) {
    this.subscription1 = this.ethcontractService.currentUser.subscribe(currentUser => this.currentUser = currentUser);
  }

  ngOnInit() {
    this.id = +this.route.snapshot.paramMap.get('id');
    this.subscription2 = this.ethcontractService.getProcess(this.id).subscribe(process => this.process = process);
    this.subscription3 = this.ethcontractService.getTasks(this.id).subscribe(tasks => this.tasks = tasks);
  }

  get hasReadEmployeesPerm() {
    return this.currentUser && (this.currentUser.role === Role.Supervisor || this.currentUser.role === Role.Director || this.currentUser.role === Role.Admin);
  }

  get hasReadSupervisorsPerm() {
    return this.currentUser && (this.currentUser.role === Role.Director || this.currentUser.role === Role.Admin);
  }

  get isEmployee() {
    return this.currentUser && this.currentUser.role === Role.Employee;
  }

  get isSupervisor() {
    return this.currentUser && this.currentUser.role === Role.Supervisor;
  }

  completeTask(processId, taskId) {  
    const modalRef = this.modalService.open(ConfirmationModalComponent);
    modalRef.componentInstance.message = 'Are you sure that you want to complete the task?';
    modalRef.result.then(confirm => {
      if(confirm){
        this.isSaving = true;
        this.ethcontractService.completeTask(processId.words[0], taskId.words[0]).then(result => {
          if(result){
            this.subscription2 = this.ethcontractService.getProcess(this.id).subscribe(process => this.process = process);
            this.subscription3 = this.ethcontractService.getTasks(this.id).subscribe(tasks => this.tasks = tasks);
          } else {
            this.snackBar.open('Please confirm transaction in order to complete the task', '', {
              duration: 10000
            });
          }
          this.isSaving = false;
        });
      }
    });
  }

  addTask(processId) {
    this.isSaving = true;
    const modalRef = this.modalService.open(AddTaskModalComponent);
    modalRef.componentInstance.processId = processId.words[0];
    modalRef.result.then(submit => {
      if(submit){
        this.subscription2 = this.ethcontractService.getProcess(this.id).subscribe(process => this.process = process);
        this.subscription3 = this.ethcontractService.getTasks(this.id).subscribe(tasks => this.tasks = tasks);
      }
      this.isSaving = false;
    });
  }

  acceptTask(processId, taskId) {
    const modalRef = this.modalService.open(ConfirmationModalComponent);
    modalRef.componentInstance.message = 'Are you sure that you want to assign yourself the task?';
    modalRef.result.then(confirm => {
      if(confirm){
        this.isSaving = true;
        this.ethcontractService.acceptTask(processId.words[0], taskId.words[0]).then(result => {
          if(result){
            this.subscription2 = this.ethcontractService.getProcess(this.id).subscribe(process => this.process = process);
            this.subscription3 = this.ethcontractService.getTasks(this.id).subscribe(tasks => this.tasks = tasks);
          } else {
            this.snackBar.open('Please confirm transaction in order to accept the task', '', {
              duration: 10000
            });
          }
          this.isSaving = false;
        });
      }
    });
  }

  completeProcess(processId) {
    const modalRef = this.modalService.open(ConfirmationModalComponent);
    modalRef.componentInstance.message = 'Are you sure that you want to complete the process?';
    modalRef.result.then(confirm => {
      if(confirm){
        this.isSaving = true;
        this.ethcontractService.completeProcess(processId.words[0]).then(result => {
          if(result){
            this.subscription2 = this.ethcontractService.getProcess(this.id).subscribe(process => this.process = process);
            this.subscription3 = this.ethcontractService.getTasks(this.id).subscribe(tasks => this.tasks = tasks);
          } else {
            this.snackBar.open('Please confirm transaction in order to complete the process', '', {
              duration: 10000
            });
          }
          this.isSaving = false;
        });
      }
    });
  }

  cancelProcess(processId) {
    const modalRef = this.modalService.open(ConfirmationModalComponent);
    modalRef.componentInstance.message = 'Are you sure that you want to cancel the process?';
    modalRef.result.then(confirm => {
      if(confirm){
        this.isSaving = true;
        this.ethcontractService.cancelProcess(processId.words[0]).then(result => {
          if(result){
            this.subscription2 = this.ethcontractService.getProcess(this.id).subscribe(process => this.process = process);
            this.subscription3 = this.ethcontractService.getTasks(this.id).subscribe(tasks => this.tasks = tasks);
          } else {
            this.snackBar.open('Please confirm transaction in order to cancel the process', '', {
              duration: 10000
            });
          }
          this.isSaving = false;
        });
      }
    });
  }

  approveProcess(processId) {
    this.isSaving = true;
    this.ethcontractService.approveProcess(processId.words[0]).then(result => {
      if(result){
        this.subscription2 = this.ethcontractService.getProcess(this.id).subscribe(process => this.process = process);
        this.subscription3 = this.ethcontractService.getTasks(this.id).subscribe(tasks => this.tasks = tasks);
      } else {
        this.snackBar.open('Please confirm transaction in order to approve the process', '', {
          duration: 10000
        });
      }
      this.isSaving = false;
    });
  }

  rejectProcess(processId) {
    this.isSaving = true;
    this.ethcontractService.rejectProcess(processId.words[0]).then(result => {
      if(result){
        this.subscription2 = this.ethcontractService.getProcess(this.id).subscribe(process => this.process = process);
        this.subscription3 = this.ethcontractService.getTasks(this.id).subscribe(tasks => this.tasks = tasks);
      } else {
        this.snackBar.open('Please confirm transaction in order to reject the process', '', {
          duration: 10000
        });
      }
      this.isSaving = false;
    });
  }

  // based on https://bearnithi.com/2019/05/25/add-remove-multiple-input-fields-dynamically-template-driven-angular/
  addField() {
    this.process.parsedDocument.push({
      name: '',
      value: ''
    });
  }

  removeField(i: number) {
    this.process.parsedDocument.splice(i, 1);
  }

  saveDocument() {
    this.isSaving = true;
    this.ethcontractService.setDocument(JSON.stringify(this.process.parsedDocument), this.id).then(result => {
      if(result){
        this.subscription2 = this.ethcontractService.getProcess(this.id).subscribe(process => this.process = process);
        this.subscription3 = this.ethcontractService.getTasks(this.id).subscribe(tasks => this.tasks = tasks);
      } else {
        this.snackBar.open('Please confirm transaction in order to save the document', '', {
          duration: 10000
        });
      }
      this.isSaving = false;
    });
  }

  canEdit(tasks) {
    return tasks[tasks.length - 1].status === 'Pending' &&
            tasks[tasks.length - 1].acceptedBy == this.currentUser.id &&
            this.isEmployee;
  }

  downloadPDF(processId, document) {
    this.pdfService.downloadPDF(processId, document);
  }

  openTaskDetailsModal(task) {
    const modalRef = this.modalService.open(TaskDetailsModalComponent);
    modalRef.componentInstance.task = task;
  }

  ngOnDestroy() {
    this.subscription1.unsubscribe();
    this.subscription2.unsubscribe();
    this.subscription3.unsubscribe();
  }

}
