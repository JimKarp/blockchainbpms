import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { EthcontractService } from 'src/app/services/ethcontract.service';
import { User } from 'src/app/models/user';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-users-table',
  templateUrl: './users-table.component.html'
})
export class UsersTableComponent implements OnInit, OnDestroy {
  @Input() currentPage: string;
  users: User[] = [];
  showInfo: boolean[] = [];
  isAsc: string;
  isDesc: string;
  isClosed: boolean;
  addBtnTxt: string;
  searchValue: string;
  private subscription: Subscription;
  
  constructor(private ethcontractService: EthcontractService) { }

  ngOnInit() {
    if(this.currentPage === "Employees"){
      this.addBtnTxt = 'Add Employee';
      this.subscription = this.ethcontractService.getEmployees().subscribe(employees => this.users = employees);
    } else if(this.currentPage === "Supervisors") {
      this.addBtnTxt = 'Add Supervisor';
      this.subscription = this.ethcontractService.getSupervisors().subscribe(supervisors => this.users = supervisors);
    } else if(this.currentPage === "Directors") {
      this.addBtnTxt = 'Add Director';
      this.subscription = this.ethcontractService.getDirectors().subscribe(directors => this.users = directors);
    }
  }

  // based on https://stackoverflow.com/questions/58820715/how-can-i-sort-html-table-using-angular-typescript-without-3rd-party
  sortTable(colName: string) {
    if (this.isAsc === colName) {
      this.isDesc = colName;
      this.isAsc = undefined;
      this.users.sort((a, b) => a[colName] < b[colName] ? 1 : a[colName] > b[colName] ? -1 : 0);
    } else {
      this.isAsc = colName;
      this.isDesc = undefined;
      this.users.sort((a, b) => a[colName] > b[colName] ? 1 : a[colName] < b[colName] ? -1 : 0);
    }
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
