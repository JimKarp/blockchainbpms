import { Component, OnInit, Input } from '@angular/core';
import { EthcontractService } from 'src/app/services/ethcontract.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html'
})
export class UsersComponent implements OnInit {
  currentPage: string;
  
  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.currentPage = this.route.snapshot.url[0].path;
    this.currentPage = this.currentPage.charAt(0).toUpperCase() + this.currentPage.substring(1);
  }

}
