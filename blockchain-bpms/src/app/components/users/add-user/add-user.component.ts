import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { EthcontractService } from 'src/app/services/ethcontract.service';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html'
})
export class AddUserComponent implements OnInit {
  currentPage: string;
  userform: FormGroup;
  formSubmitted: boolean = false;
  isSaving = false;

  constructor(
    private snackBar: MatSnackBar,
    private ethcontractService: EthcontractService, 
    private route: ActivatedRoute,
    private location: Location,
    private router: Router,
    private fb: FormBuilder
  ) {
    this.currentPage = this.route.snapshot.url[0].path;
    this.currentPage = this.currentPage.slice(0, -1);
    this.currentPage = this.currentPage.charAt(0).toUpperCase() + this.currentPage.substring(1);
  }

  ngOnInit() {
    this.userform = this.fb.group({
      name: ['', Validators.required],
      surname: ['', Validators.required],
      walletAddress: ['', [Validators.required, Validators.pattern('^0x[a-zA-Z0-9]{40}')]],
      department: ['', Validators.required]
    });
  }

  previousPage() {
    this.location.back();
  }

  onSubmit() {
    if (this.userform.valid) {
      this.isSaving = true;
      this.formSubmitted = true;
      if (this.currentPage === 'Employee') {
        this.ethcontractService.addEmployee(this.userform.value.walletAddress, this.userform.value.name, this.userform.value.surname, this.userform.value.department).then(result => {
          if(result === -1){
            this.snackBar.open('Please confirm transaction in order to add the employee', '', {
              duration: 10000
            });
          }else if(result){
            this.snackBar.open('Employee added successfully', '', {
              duration: 3000
            });
            this.router.navigate(['/employees', result]);
          } else {
            this.snackBar.open('User already exists. Please use a different wallet address', '', {
              duration: 10000
            });
          }
          this.isSaving = false;
        });
      } else if(this.currentPage === 'Supervisor') {
        this.ethcontractService.addSupervisor(this.userform.value.walletAddress, this.userform.value.name, this.userform.value.surname, this.userform.value.department).then(result => {
          if(result === -1){
            this.snackBar.open('Please confirm transaction in order to add the supervisor', '', {
              duration: 10000
            });
          }else if(result){
            this.snackBar.open('Supervisor added successfully', '', {
              duration: 3000
            });
            this.router.navigate(['/supervisors', result]);
          } else {
            this.snackBar.open('User already exists. Please use a different wallet address', '', {
              duration: 10000
            });
          }
          this.isSaving = false;
        });
      } else if(this.currentPage === 'Director') {
        this.ethcontractService.addDirector(this.userform.value.walletAddress, this.userform.value.name, this.userform.value.surname, this.userform.value.department).then(result => {
          if(result === -1){
            this.snackBar.open('Please confirm transaction in order to add the director', '', {
              duration: 10000
            });
          }else if(result){
            this.snackBar.open('Director added successfully', '', {
              duration: 3000
            });
            this.router.navigate(['/directors', result]);
          } else {
            this.snackBar.open('User already exists. Please use a different wallet address', '', {
              duration: 10000
            });
          }
          this.isSaving = false;
        });
      }
    }
  }

}
