import { Component, OnInit, OnDestroy } from '@angular/core';
import { EthcontractService } from 'src/app/services/ethcontract.service';
import { ActivatedRoute } from '@angular/router';
import { User } from 'src/app/models/user';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Clipboard } from "@angular/cdk/clipboard"
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html'
})
export class UserProfileComponent implements OnInit, OnDestroy {
  user: User;
  id: number;
  currentPage: string;
  editMode: boolean = false;
  department: string;
  amount: number;
  private subscription: Subscription;

  constructor(
    private clipboard: Clipboard,
    private snackBar: MatSnackBar,
    private ethcontractService: EthcontractService,
    private route: ActivatedRoute
  ) {
    this.id = +this.route.snapshot.paramMap.get('id');
    this.currentPage = this.route.snapshot.url[0].path;
    if (this.currentPage === 'employees') {
      this.currentPage = 'Employee';
    } else if (this.currentPage === 'supervisors') {
      this.currentPage = 'Supervisor';
    } else if (this.currentPage === 'directors') {
      this.currentPage = 'Director';
    }
  }

  ngOnInit() {
    if (this.currentPage === 'Employee'){
      this.subscription = this.ethcontractService.getEmployee(this.id).subscribe(employee => this.user = employee);
    } else if(this.currentPage === 'Supervisor'){
      this.subscription = this.ethcontractService.getSupervisor(this.id).subscribe(supervisor => this.user = supervisor);
    } else if(this.currentPage === 'Director'){
      this.subscription = this.ethcontractService.getDirector(this.id).subscribe(director => this.user = director);
    }
  }

  toggleEdit() {
    this.editMode = !this.editMode;
    this.department = this.user.department;
  }

  saveChanges() {
    if((this.user.department !== this.department)){
      if (this.currentPage === 'Employee'){
        this.ethcontractService.editEmployee(this.id, this.department).then(result => {
          if(result){
            this.snackBar.open('Changes saved successfully', '', {
              duration: 3000
            });
          } else {
            this.snackBar.open('Error saving changes', '', {
              duration: 3000
            });
          }
        });
      } else if(this.currentPage === 'Supervisor'){
        this.ethcontractService.editSupervisor(this.id, this.department).then(result => {
          if(result){
            this.snackBar.open('Changes saved successfully', '', {
              duration: 3000
            });
          } else {
            this.snackBar.open('Error saving changes', '', {
              duration: 3000
            });
          }
        });
      } else if(this.currentPage === 'Director'){
        this.ethcontractService.editDirector(this.id, this.department).then(result => {
          if(result){
            this.snackBar.open('Changes saved successfully', '', {
              duration: 3000
            });
          } else {
            this.snackBar.open('Error saving changes', '', {
              duration: 3000
            });
          }
        });
      }
      this.user.department = this.department;
    }
    this.editMode = false;
  }

  // based on https://medium.com/angular-in-depth/use-the-new-angular-clipboard-cdk-to-interact-with-the-clipboard-be1c9c94cac2
  copyToClipboard(value: string){
    this.clipboard.copy(value)
    this.snackBar.open('Address copied to clipboard', '', {
      duration: 3000
    });
  }

  transferEther(walletAddress, amount) {
    if(amount){
      this.ethcontractService.transferEther(walletAddress, amount).then(result => {
        if(result){
          this.snackBar.open('Amount of ether transfered successfully', '', {
            duration: 3000
          });
          this.amount = undefined;
          if (this.currentPage === 'Employee'){
            this.subscription = this.ethcontractService.getEmployee(this.id).subscribe(employee => this.user = employee);
          } else if(this.currentPage === 'Supervisor'){
            this.subscription = this.ethcontractService.getSupervisor(this.id).subscribe(supervisor => this.user = supervisor);
          } else if(this.currentPage === 'Director'){
            this.subscription = this.ethcontractService.getDirector(this.id).subscribe(director => this.user = director);
          }
        } else {
          this.snackBar.open('Please confirm transaction in order to transfer the amount of ether', '', {
            duration: 10000
          });
        }
      });
    } else {
      this.snackBar.open('Please fill an amount of ether to transfer', '', {
        duration: 10000
      });
    }
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
