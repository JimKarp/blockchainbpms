import { Component, OnInit, OnDestroy } from '@angular/core';
import { EthcontractService } from 'src/app/services/ethcontract.service';
import { User } from 'src/app/models/user';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html'
})
export class HomeComponent implements OnInit, OnDestroy {
  currentUser: User;
  private subscription: Subscription;

  constructor(private ethcontractService: EthcontractService) { 
    this.subscription = this.ethcontractService.currentUser.subscribe(currentUser => this.currentUser = currentUser);
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
