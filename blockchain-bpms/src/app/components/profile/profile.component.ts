import { Component, OnInit, OnDestroy } from '@angular/core';
import { EthcontractService } from 'src/app/services/ethcontract.service';
import { User } from 'src/app/models/user';
import { Role } from 'src/app/models/role';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Clipboard } from "@angular/cdk/clipboard"
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html'
})
export class ProfileComponent implements OnInit, OnDestroy {
  currentUser: User;
  user: User;
  balance: number = 0;
  private subscription1: Subscription;
  private subscription2: Subscription;

  constructor(
    private clipboard: Clipboard,
    private snackBar: MatSnackBar,
    private ethcontractService: EthcontractService
  ) {
    this.subscription1 = this.ethcontractService.currentUser.subscribe(currentUser => this.currentUser = currentUser);
  }

  ngOnInit() {
    this.ethcontractService.getAccountBalance(this.currentUser.walletAddress).then(balance => this.balance = balance);
    if(this.currentUser.role === Role.Employee){
      this.subscription2 = this.ethcontractService.getEmployee(this.currentUser.id).subscribe(employee => this.user = employee);
    }else if(this.currentUser.role === Role.Supervisor){
      this.subscription2 = this.ethcontractService.getSupervisor(this.currentUser.id).subscribe(supervisor => this.user = supervisor);
    }else if(this.currentUser.role === Role.Director){
      this.subscription2 = this.ethcontractService.getDirector(this.currentUser.id).subscribe(director => this.user = director);
    }
  }

  // based on https://medium.com/angular-in-depth/use-the-new-angular-clipboard-cdk-to-interact-with-the-clipboard-be1c9c94cac2
  copyToClipboard(value: string){
    this.clipboard.copy(value)
    this.snackBar.open('Address copied to clipboard', '', {
      duration: 3000
    });
  }

  ngOnDestroy() {
    this.subscription1.unsubscribe();
    if(this.subscription2) this.subscription2.unsubscribe();
  }

}
