import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of, Subject } from 'rxjs';
import * as properties from 'src/app/properties/properties.json';

@Injectable()
export class DataService {
  navbarItems: any;
  private _listener = new Subject<any>();

  constructor(private http: HttpClient) { }

  getNavbarItems(role: string): Observable<any>{
    this.navbarItems = [];
    properties.navbars.forEach(navbar => {
      if(navbar.role === role){
        this.navbarItems = navbar.navbarItems;
      }
    });
    return of(this.navbarItems);
  }

  // based on https://stackoverflow.com/questions/847185/convert-a-unix-timestamp-to-time-in-javascript
  getFormattedTimestamp(timestamp: number): string{
    if (timestamp > 946731600) {
      const date = new Date(timestamp * 1000);
      const day = date.getDate();
      const month = date.getMonth() + 1;
      const year = date.getFullYear();
      const hours = date.getHours();
      const minutes = "0" + date.getMinutes();
      const formattedTimestamp = day + '/' + month + '/' + year + ' ' + hours + ':' + minutes.substr(-2);
      return formattedTimestamp;
    } else {
      return null;
    }
  }
  
  // based on https://stackoverflow.com/questions/13903897/javascript-return-number-of-days-hours-minutes-seconds-between-two-dates
  getDuration(start: number, end: number): string {
    let delta = Math.abs(end - start);
    let fortmattedDuration;

    // calculate (and subtract) whole days
    const days = Math.floor(delta / 86400);
    delta -= days * 86400;

    // calculate (and subtract) whole hours
    const hours = Math.floor(delta / 3600) % 24;
    delta -= hours * 3600;

    // calculate (and subtract) whole minutes
    const minutes = Math.floor(delta / 60) % 60;
    delta -= minutes * 60;

    // what's left is seconds
    const seconds = delta % 60;

    if(days) {
      fortmattedDuration = `${days} days, ${hours} hours`;
    } else if(hours) {
      fortmattedDuration = `${hours} hours, ${minutes} minutes`;
    } else if(minutes) {
      fortmattedDuration = `${minutes} minutes, ${seconds} seconds`;
    } else if(seconds) {
      fortmattedDuration = `${seconds} seconds`;
    }

    return fortmattedDuration;
  }
  
}
