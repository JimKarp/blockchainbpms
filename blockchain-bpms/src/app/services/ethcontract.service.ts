import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject, of } from 'rxjs';
import { User } from 'src/app/models/user';
import { Process } from 'src/app/models/process';
import { Task } from 'src/app/models/task';
import { DataService } from './data.service';
import { Router } from '@angular/router';
const Web3 = require('web3');
const TruffleContract = require('truffle-contract');

declare let require: any;
declare let window: any;

const tokenAbi = require('../../../../build/contracts/Blockchain_BPMS.json');
const tokenAbiTransfer = require('../../../../build/contracts/Blockchain_BPMS_Transfer.json');

@Injectable({ providedIn: 'root' })
export class EthcontractService {
  private currentUserSubject: BehaviorSubject<User> = new BehaviorSubject<User>(null);
  public currentUser: Observable<User>;
  private readonly web3: any;
  private enable: any;
  currentUserId: any;

  constructor(
    private router: Router,
    private dataService: DataService
  ) {
    this.currentUser = this.currentUserSubject.asObservable();
    if (window.ethereum === undefined) {
      this.router.navigate(['/metamask']);
    } else {
      if (typeof window.web3 !== 'undefined') {
        this.web3 = window.web3.currentProvider;
      } else {
        this.web3 = new Web3.providers.HttpProvider('http://localhost:8545');
      }
      window.web3 = new Web3(this.web3);
      this.enable = this.enableMetaMaskAccount();
      // this.accountListener();
    }
  }

  public get currentUserValue(): User {
    return this.currentUserSubject.value;
  }

  async currentUserValueAsPromise() {
    if(this.currentUserSubject.value) {
      return this.currentUserSubject.value;
    } else {
      await this.accountInfo();
      return this.currentUserSubject.value;
    }
  }

  private async enableMetaMaskAccount(): Promise<any> {
    let enable = false;
    await new Promise((resolve, reject) => {
      enable = window.ethereum.enable();
    });
    return Promise.resolve(enable);
  }

  private async accountInfo() {
    return await new Promise((resolve) => {
      let curr_user = new User();
      window.web3.eth.getCoinbase((error, account) => {
        if (error) {
          console.log(error);
          resolve(curr_user);
        } else {
          if(account){
            let blockchainBPMSContract = TruffleContract(tokenAbi);
            blockchainBPMSContract.setProvider(this.web3);
            blockchainBPMSContract.deployed().then(instance => {
              instance.getUser(window.web3.utils.toChecksumAddress(account)).then(user => {
                if(window.web3.utils.toChecksumAddress(account) === user[0]){
                  curr_user.walletAddress = user[0];
                  curr_user.role = user[1];
                  curr_user.id = +user[2];
                  this.currentUserSubject.next(curr_user);
                  resolve(curr_user);
                } else {
                  resolve(curr_user);
                }
              });     
            }).catch(function (error) {
              console.log(error);
              resolve(curr_user);
            });
          } else {
            resolve(curr_user);
          }
        }
      });
    });
  }

  // based on https://ethereum.stackexchange.com/questions/42768/how-can-i-detect-change-in-account-in-metamask
  private accountListener() {
    window.ethereum.on('accountsChanged', accounts => {
        location.replace('/');
    });
  }

  async getAccountBalance(walletAddress): Promise<any> {
    return await new Promise((resolve) => {
      window.web3.eth.getBalance(walletAddress, (err, balance) => {
        if (err === null) {
          resolve(parseFloat(window.web3.utils.fromWei(balance, "ether")));
        } else {
          resolve(0);
        }
      });
    });
  }

  getAccountAddress() {
    return new Promise((resolve, reject) => {
      window.web3.eth.getCoinbase((err, account) => {
        if (err === null) {
          return resolve({ address: account });
        } else {
          return reject({ address: "error" });
        }
      });
    });
  }

  async addEmployee(employeeAddress: any, name: string, surname: string, department: string): Promise<number> {
    return await new Promise((resolve) => {
      let blockchainBPMSContract = TruffleContract(tokenAbi);
      blockchainBPMSContract.setProvider(this.web3);
      blockchainBPMSContract.deployed().then(instance => {
        this.getAccountAddress().then((account: any) => {
          instance.addEmployee(employeeAddress, name, surname, department, { from: account.address }).then(result => {
            if (result.logs.length){
              resolve(result.logs[0].args.employeeId.words[0])
            }
          }).catch(function (error) {
            resolve(-1);
          });
        });
      }).catch(function (error) {
        console.log(error);
      });
    });
  }

  async addSupervisor(supervisorAddress: any, name: string, surname: string, department: string): Promise<number> {
    return await new Promise((resolve) => {
      let blockchainBPMSContract = TruffleContract(tokenAbi);
      blockchainBPMSContract.setProvider(this.web3);
      blockchainBPMSContract.deployed().then(instance => {
        this.getAccountAddress().then((account: any) => {
          instance.addSupervisor(supervisorAddress, name, surname, department, { from: account.address }).then(result => {
            if (result.logs.length){
              resolve(result.logs[0].args.supervisorId.words[0])
            }
          }).catch(function (error) {
            resolve(-1);
          });
        }).catch;
      }).catch(function (error) {
        console.log(error);
      });
    });
  }

  async addDirector(directorAddress: any, name: string, surname: string, department: string): Promise<number> {
    return await new Promise((resolve) => {
      let blockchainBPMSContract = TruffleContract(tokenAbi);
      blockchainBPMSContract.setProvider(this.web3);
      blockchainBPMSContract.deployed().then(instance => {
        this.getAccountAddress().then((account: any) => {
          instance.addDirector(directorAddress, name, surname, department, { from: account.address }).then(result => {
            if (result.logs.length){
              resolve(result.logs[0].args.directorId.words[0])
            }
          }).catch(function (error) {
            resolve(-1);
          });
        });
      }).catch(function (error) {
        console.log(error);
      });
    });
  }

  async generateProcess(processName: string, firstTaskName: string): Promise<number> {
    return await new Promise((resolve, reject) => {
      let currentUserId = this.currentUserValue.id;
      let blockchainBPMSContract = TruffleContract(tokenAbi);
      blockchainBPMSContract.setProvider(this.web3);
      blockchainBPMSContract.deployed().then(instance => {
        this.getAccountAddress().then((account: any) => {
          instance.generateProcess(processName, currentUserId, firstTaskName, { from: account.address }).then(result => {
            if (result.logs.length){
              resolve(result.logs[0].args.processId.words[0])
            }
          }).catch(function (error) {
            resolve(-1);
          });
        });
      }).catch(function (error) {
        console.log(error);
      });
    });
  }

  async setDocument(document: string, processId: number): Promise<boolean> {
    return await new Promise((resolve, reject) => {
      let currentUserId = this.currentUserValue.id;
      let blockchainBPMSContract = TruffleContract(tokenAbi);
      blockchainBPMSContract.setProvider(this.web3);
      blockchainBPMSContract.deployed().then(instance => {
        this.getAccountAddress().then((account: any) => {
          instance.setDocument(document, processId, currentUserId, { from: account.address }).then(result => {
            resolve(!!result)
          }).catch(function (error) {
            resolve(false);
          });
        });
      }).catch(function (error) {
        console.log(error);
      });
    });
  }

  getEmployee(employeeId: number): Observable<User> {
    let _user = new User();
    let blockchainBPMSContract = TruffleContract(tokenAbi);
    blockchainBPMSContract.setProvider(this.web3);
    blockchainBPMSContract.deployed().then(instance => {
      instance.getEmployee(employeeId).then(employee => {
        _user.id = employee[0];
        _user.walletAddress = employee[1];
        _user.name = employee[2];
        _user.surname = employee[3];
        _user.department = employee[4];
        _user.tasksCompleted = employee[5];
        _user.tasksPending = employee[6];
        _user.contributions = employee[7];
        this.getAccountBalance(employee[1]).then(balance => _user.balance = balance);
      });
    }).catch(function (error) {
      console.log(error);
    });
    return of(_user);
  }

  getSupervisor(supervisorId: number): Observable<User> {
    let _user = new User();
    let blockchainBPMSContract = TruffleContract(tokenAbi);
    blockchainBPMSContract.setProvider(this.web3);
    blockchainBPMSContract.deployed().then(instance => {
      instance.getSupervisor(supervisorId).then(supervisor => {
        _user.id = supervisor[0];
        _user.walletAddress = supervisor[1];
        _user.name = supervisor[2];
        _user.surname = supervisor[3];
        _user.department = supervisor[4];
        _user.processesApproved = supervisor[5];
        _user.processesRejected = supervisor[6];
        this.getAccountBalance(supervisor[1]).then(balance => _user.balance = balance);
      });
    }).catch(function (error) {
      console.log(error);
    });
    return of(_user);
  }

  getDirector(directorId: number): Observable<User> {
    let _user = new User();
    let blockchainBPMSContract = TruffleContract(tokenAbi);
    blockchainBPMSContract.setProvider(this.web3);
    blockchainBPMSContract.deployed().then(instance => {
      instance.getDirector(directorId).then(director => {
        _user.id = director[0];
        _user.walletAddress = director[1];
        _user.name = director[2];
        _user.surname = director[3];
        _user.department = director[4];
        this.getAccountBalance(director[1]).then(balance => _user.balance = balance);
      });
    }).catch(function (error) {
      console.log(error);
    });
    return of(_user);
  }

  getEmployees(): Observable<User[]> {
    let users: User[] = [];
    let blockchainBPMSContract = TruffleContract(tokenAbi);
    blockchainBPMSContract.setProvider(this.web3);
    blockchainBPMSContract.deployed().then(instance => {
      instance.employeeId.call().then(async employeesCounter => {
        for (let i = employeesCounter; i >= 1; i--) {
          await instance.getEmployee(i).then(employee => {
            let _user = new User();
            _user.id = employee[0];
            _user.walletAddress = employee[1];
            _user.name = employee[2];
            _user.surname = employee[3];
            _user.department = employee[4];
            _user.tasksCompleted = employee[5];
            _user.tasksPending = employee[6];
            _user.contributions = employee[7];
            this.getAccountBalance(employee[1]).then(balance => _user.balance = balance);
            users.push(_user);
          });
        }
      });
    }).catch(function (error) {
      console.log(error);
    });
    return of(users);
  }

  getSupervisors(): Observable<User[]> {
    let users: User[] = [];
    let blockchainBPMSContract = TruffleContract(tokenAbi);
    blockchainBPMSContract.setProvider(this.web3);
    blockchainBPMSContract.deployed().then(instance => {
      instance.supervisorId.call().then(async supervisorsCounter => {
        for (let i = supervisorsCounter; i >= 1; i--) {
          await instance.getSupervisor(i).then(supervisor => {
            let _user = new User();
            _user.id = supervisor[0];
            _user.walletAddress = supervisor[1];
            _user.name = supervisor[2];
            _user.surname = supervisor[3];
            _user.department = supervisor[4];
            _user.processesApproved = supervisor[5];
            _user.processesRejected = supervisor[6];
            this.getAccountBalance(supervisor[1]).then(balance => _user.balance = balance);
            users.push(_user);
          });
        }
      });
    }).catch(function (error) {
      console.log(error);
    });
    return of(users);
  }

  getDirectors(): Observable<User[]> {
    let users: User[] = [];
    let blockchainBPMSContract = TruffleContract(tokenAbi);
    blockchainBPMSContract.setProvider(this.web3);
    blockchainBPMSContract.deployed().then(instance => {
      instance.directorId.call().then(async directorsCounter => {
        for (let i = directorsCounter; i >= 1; i--) {
          await instance.getDirector(i).then(director => {
            let _user = new User();
            _user.id = director[0];
            _user.walletAddress = director[1];
            _user.name = director[2];
            _user.surname = director[3];
            _user.department = director[4];
            this.getAccountBalance(director[1]).then(balance => _user.balance = balance);
            users.push(_user);
          });
        }
      });
    }).catch(function (error) {
      console.log(error);
    });
    return of(users);
  }

  getProcesses(): Observable<Process[]> {
    let processes: Process[] = [];
    let blockchainBPMSContract = TruffleContract(tokenAbi);
    blockchainBPMSContract.setProvider(this.web3);
    blockchainBPMSContract.deployed().then(instance =>{
      instance.processId.call().then(async processesCounter => {
        for (let i = processesCounter; i >= 1; i--) {
          await instance.getProcess(i).then(process => {
            let _process = new Process();
            _process.processId = process[0];
            _process.processName = process[1];
            _process.generatedBy = process[2];
            _process.timestampGenerated = process[3];
            _process.formattedTimestampGenerated = this.dataService.getFormattedTimestamp(process[3]);
            _process.endedBy = process[4];
            _process.timestampEnded = process[5];
            _process.checkedBy = process[6];
            _process.timestampChecked = process[7];
            _process.status = process[8];
            _process.tasksCounter = process[9];
            _process.document = process[10];
            processes.push(_process);
          });
        }
      });
    }).catch(function (error) {
      console.log(error);
    });
    return of(processes);
  }

  getProcess(processId: number): Observable<Process> {
    let _process = new Process();
    let blockchainBPMSContract = TruffleContract(tokenAbi);
    blockchainBPMSContract.setProvider(this.web3);
    blockchainBPMSContract.deployed().then(instance => {
      instance.getProcess(processId).then(process => {
        _process.processId = process[0];
        _process.processName = process[1];
        _process.generatedBy = process[2];
        instance.getEmployee(process[2]).then(employee => {
          let generatedByEmployee = new User();
          generatedByEmployee.name = employee[2];
          generatedByEmployee.surname = employee[3];
          _process.generatedByEmployee = generatedByEmployee;
        });
        _process.timestampGenerated = process[3];
        _process.formattedTimestampGenerated = this.dataService.getFormattedTimestamp(process[3]);
        _process.endedBy = process[4];
        instance.getEmployee(process[4]).then(employee => {
          let endedByEmployee = new User();
          endedByEmployee.name = employee[2];
          endedByEmployee.surname = employee[3];
          _process.endedByEmployee = endedByEmployee;
        });
        _process.timestampEnded = process[5];
        _process.formattedTimestampEnded = this.dataService.getFormattedTimestamp(process[5]);
        _process.checkedBy = process[6];
        instance.getSupervisor(process[6]).then(supervisor => {
          let checkedBySupervisor = new User();
          checkedBySupervisor.name = supervisor[2];
          checkedBySupervisor.surname = supervisor[3];
          _process.checkedBySupervisor = checkedBySupervisor;
        });
        _process.timestampChecked = process[7];
        _process.formattedTimestampChecked = this.dataService.getFormattedTimestamp(process[7]);
        _process.status = process[8];
        _process.tasksCounter = process[9];
        _process.document = process[10];
        _process.parsedDocument = 
          (process[10] && JSON.parse(process[10]).length)
          ? JSON.parse(process[10])
          : [{
            name: '',
            value: ''
          }];
      });
    }).catch(function (error) {
      console.log(error);
    });
    return of(_process);
  }

  getTasks(processId: number): Observable<Task[]> {
    let tasks: Task[] = [];
    let blockchainBPMSContract = TruffleContract(tokenAbi);
    blockchainBPMSContract.setProvider(this.web3);
    blockchainBPMSContract.deployed().then(instance =>{
      instance.getTasksCounter(processId).then(async tasksCounter => {
        for (let i = 1; i <= tasksCounter; i++) {
          await instance.getTask(processId, i).then(task => {
            let _task = new Task();
            _task.taskId = task[0];
            _task.taskName = task[1];
            _task.generatedBy = task[2];
            instance.getEmployee(task[2]).then(employee => {
              let generatedByEmployee = new User();
              generatedByEmployee.name = employee[2];
              generatedByEmployee.surname = employee[3];
              _task.generatedByEmployee = generatedByEmployee;
            });
            _task.timestampGenerated = task[3];
            _task.formattedTimestampGenerated = this.dataService.getFormattedTimestamp(task[3]);
            _task.acceptedBy = task[4];
            instance.getEmployee(task[4]).then(employee => {
              let acceptedByEmployee = new User();
              acceptedByEmployee.name = employee[2];
              acceptedByEmployee.surname = employee[3];
              _task.acceptedByEmployee = acceptedByEmployee;
            });
            _task.timestampAccepted = task[5];
            _task.formattedTimestampAccepted = this.dataService.getFormattedTimestamp(task[5]);
            _task.completedBy = task[6];
            instance.getEmployee(task[6]).then(employee => {
              let completedByEmployee = new User();
              completedByEmployee.name = employee[2];
              completedByEmployee.surname = employee[3];
              _task.completedByEmployee = completedByEmployee;
            });
            _task.timestampCompleted = task[7];
            _task.formattedTimestampCompleted = this.dataService.getFormattedTimestamp(task[7]);
            if(task[8] === 'Completed') {
              _task.duration = this.dataService.getDuration(task[3], task[7]);
            }
            _task.status = task[8];
            _task.processId = processId;
            tasks.push(_task);
          });
        }
      });
    }).catch(function (error) {
      console.log(error);
    });
    return of(tasks);
  }

  getOpenTasks(): Observable<Task[]> {
    let tasks: Task[] = [];
    let blockchainBPMSContract = TruffleContract(tokenAbi);
    blockchainBPMSContract.setProvider(this.web3);
    blockchainBPMSContract.deployed().then(instance =>{
      instance.processId.call().then(async processesCounter => {
        for (let i = processesCounter; i >= 1; i--) {
          await instance.getProcess(i).then(process => {
            if(process[8]==='Pending'){
              instance.getTasksCounter(process[0]).then(async tasksCounter => {
                await instance.getTask(process[0], tasksCounter).then(task => {
                  if (task[8]==='Open'){
                    let _task = new Task();
                    _task.taskId = task[0];
                    _task.taskName = task[1];
                    _task.generatedBy = task[2];
                    _task.timestampGenerated = task[3];
                    _task.formattedTimestampGenerated = this.dataService.getFormattedTimestamp(task[3]);
                    _task.acceptedBy = task[4];
                    _task.timestampAccepted = task[5];
                    _task.formattedTimestampAccepted = this.dataService.getFormattedTimestamp(task[5]);
                    _task.completedBy = task[6];
                    _task.timestampCompleted = task[7];
                    _task.formattedTimestampCompleted = this.dataService.getFormattedTimestamp(task[7]);
                    _task.status = task[8];
                    _task.processId = process[0];
                    _task.processName = process[1]
                    tasks.push(_task);
                  }
                });
              });
            }
          });
        }
      });
    }).catch(function (error) {
      console.log(error);
    });
    return of(tasks);
  }

  async editEmployee(employeeId: number, department: string): Promise<boolean> {
    return await new Promise((resolve, reject) => {
      let blockchainBPMSContract = TruffleContract(tokenAbi);
      blockchainBPMSContract.setProvider(this.web3);
      blockchainBPMSContract.deployed().then(instance => {
        this.getAccountAddress().then((account: any) => {
          instance.editEmployee(employeeId, department, { from: account.address }).then(status => {
            if (!status) {
              reject(false);
            } else {
              resolve(true);
            }
          });
        });
      }).catch(function (error) {
        console.log(error);
      });
    });
  }

  async editSupervisor(supervisorId: number, department: string): Promise<boolean> {
    return await new Promise((resolve, reject) => {
      let blockchainBPMSContract = TruffleContract(tokenAbi);
      blockchainBPMSContract.setProvider(this.web3);
      blockchainBPMSContract.deployed().then(instance => {
        this.getAccountAddress().then((account: any) => {
          instance.editSupervisor(supervisorId, department, { from: account.address }).then(status => {
            if (!status) {
              reject(false);
            } else {
              resolve(true);
            }
          });
        });
      }).catch(function (error) {
        console.log(error);
      });
    });
  }

  async editDirector(directorId: number, department: string): Promise<boolean> {
    return await new Promise((resolve, reject) => {
      let blockchainBPMSContract = TruffleContract(tokenAbi);
      blockchainBPMSContract.setProvider(this.web3);
      blockchainBPMSContract.deployed().then(instance => {
        this.getAccountAddress().then((account: any) => {
          instance.editDirector(directorId, department, { from: account.address }).then(status => {
            if (!status) {
              reject(false);
            } else {
              resolve(true);
            }
          });
        });
      }).catch(function (error) {
        console.log(error);
      });
    });
  }

  async addTask(processId: number, taskName: string, accept: boolean): Promise<boolean> {
    return await new Promise((resolve, reject) => {
      let currentUserId = this.currentUserValue.id;
      let blockchainBPMSContract = TruffleContract(tokenAbi);
      blockchainBPMSContract.setProvider(this.web3);
      blockchainBPMSContract.deployed().then(instance => {
        this.getAccountAddress().then((account: any) => {
          instance.addTask(processId, taskName, currentUserId, accept, { from: account.address }).then(result => {
            resolve(!!result)
          }).catch(function (error) {
            resolve(false);
          });
        });
      }).catch(function (error) {
        console.log(error);
      });
    });
  }

  async completeTask(processId: number, taskId: number): Promise<boolean> {
    return await new Promise((resolve) => {
      let currentUserId = this.currentUserValue.id;
      let blockchainBPMSContract = TruffleContract(tokenAbi);
      blockchainBPMSContract.setProvider(this.web3);
      blockchainBPMSContract.deployed().then(instance => {
        this.getAccountAddress().then((account: any) => {
          instance.completeTask(processId, taskId, currentUserId, { from: account.address }).then(result => {
            resolve(!!result)
          }).catch(function (error) {
            resolve(false);
          });
        });
      }).catch(function (error) {
        console.log(error);
      });
    });
  }

  async acceptTask(processId: number, taskId: number): Promise<boolean> {
    return await new Promise((resolve) => {
      let currentUserId = this.currentUserValue.id;
      let blockchainBPMSContract = TruffleContract(tokenAbi);
      blockchainBPMSContract.setProvider(this.web3);
      blockchainBPMSContract.deployed().then(instance => {
        this.getAccountAddress().then((account: any) => {
          instance.acceptTask(processId, taskId, currentUserId, { from: account.address }).then(result => {
            resolve(!!result)
          }).catch(function (error) {
            resolve(false);
          });
        });
      }).catch(function (error) {
        console.log(error);
      });
    });
  }

  async completeProcess(processId: number): Promise<boolean> {
    return await new Promise((resolve) => {
      let currentUserId = this.currentUserValue.id;
      let blockchainBPMSContract = TruffleContract(tokenAbi);
      blockchainBPMSContract.setProvider(this.web3);
      blockchainBPMSContract.deployed().then(instance => {
        this.getAccountAddress().then((account: any) => {
          instance.completeProcess(processId, currentUserId, { from: account.address }).then(result => {
            resolve(!!result)
          }).catch(function (error) {
            resolve(false);
          });
        });
      }).catch(function (error) {
        console.log(error);
      });
    });
  }

  async cancelProcess(processId: number): Promise<boolean> {
    return await new Promise((resolve) => {
      let currentUserId = this.currentUserValue.id;
      let blockchainBPMSContract = TruffleContract(tokenAbi);
      blockchainBPMSContract.setProvider(this.web3);
      blockchainBPMSContract.deployed().then(instance => {
        this.getAccountAddress().then((account: any) => {
          instance.cancelProcess(processId, currentUserId, { from: account.address }).then(result => {
            resolve(!!result)
          }).catch(function (error) {
            resolve(false);
          });
        });
      }).catch(function (error) {
        console.log(error);
      });
    });
  }
  
  async approveProcess(processId: number): Promise<boolean> {
    return await new Promise((resolve) => {
      let currentUserId = this.currentUserValue.id;
      let blockchainBPMSContract = TruffleContract(tokenAbi);
      blockchainBPMSContract.setProvider(this.web3);
      blockchainBPMSContract.deployed().then(instance => {
        this.getAccountAddress().then((account: any) => {
          instance.approveProcess(processId, currentUserId, { from: account.address }).then(result => {
            resolve(!!result)
          }).catch(function (error) {
            resolve(false);
          });
        });
      }).catch(function (error) {
        console.log(error);
      });
    });
  }

  async rejectProcess(processId: number): Promise<boolean> {
    return await new Promise((resolve) => {
      let currentUserId = this.currentUserValue.id;
      let blockchainBPMSContract = TruffleContract(tokenAbi);
      blockchainBPMSContract.setProvider(this.web3);
      blockchainBPMSContract.deployed().then(instance => {
        this.getAccountAddress().then((account: any) => {
          instance.rejectProcess(processId, currentUserId, { from: account.address }).then(result => {
            resolve(!!result)
          }).catch(function (error) {
            resolve(false);
          });
        });
      }).catch(function (error) {
        console.log(error);
      });
    });
  }
  

  async transferEther(transferTo: any, amount: number) {
    return await new Promise((resolve) => {
      let blockchainBPMSTransferContract = TruffleContract(tokenAbiTransfer);
      blockchainBPMSTransferContract.setProvider(this.web3);
      blockchainBPMSTransferContract.deployed().then(instance => {
        this.getAccountAddress().then((account: any) => {
          instance.transferFund(transferTo, { from: account.address, value: window.web3.utils.toWei(amount.toString(), 'ether') }).then(result => {
            resolve(!!result)
          }).catch(function (error) {
            resolve(false);
          });
        });
      }).catch(function (error) {
        console.log(error);
      });
    });
  }
}
