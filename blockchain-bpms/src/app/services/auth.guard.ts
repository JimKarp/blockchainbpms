import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { EthcontractService } from './ethcontract.service';

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {
    currentUser: any;
    constructor(
        private router: Router,
        private ethcontractService: EthcontractService
    ) { }

    async canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        await this.ethcontractService.currentUserValueAsPromise().then(result => this.currentUser = result);
        if (this.currentUser) {
            //check if route is restricted by role
            if (route.data.roles && route.data.roles.indexOf(this.currentUser.role) === -1) {
                // role not authorised so redirect to home page
                this.router.navigate(['/']);
                return false;
            }

            //authorised so return true
            return true;
        }

        // not logged in so redirect to metamask about page with the return url
        this.router.navigate(['/metamask']);
        return false;
    }
}