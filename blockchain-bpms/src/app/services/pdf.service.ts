import { Injectable } from '@angular/core';
import { jsPDF } from 'jspdf';
import { logo_data_url } from 'src/assets/logo_data_url';

@Injectable({ providedIn: 'root' })
export class PdfService {
    doc: jsPDF;
    degreeLevel: string;
    logo = logo_data_url;
    
    constructor() { }

    downloadPDF(processId, document){
        this.doc = new jsPDF();
        this.drawHeader();
        let currentPage = 1;
        this.drawPageNumber(currentPage);
        let hAxis = 15;
        let yAxis = 60;
        this.doc.setFont('helvetica');
        this.doc.setFontSize(12);
        for(let documentItem of document) {
            if(yAxis > 270){
                this.doc.addPage();
                this.drawHeader();
                currentPage++;
                this.drawPageNumber(currentPage);
                yAxis = 60;
                this.doc.setFont('helvetica');
                this.doc.setFontSize(12);
            }
            let extraLines = 0;
            this.doc.setFont('helvetica', 'bold')
            if(documentItem.name.length < 25){
                this.doc.text(documentItem.name, hAxis, yAxis);
            } else {
                const { outputString, breakLinesCounter } = this.splitBigString(documentItem.name, 25);
                this.doc.text(`${outputString}`, hAxis, yAxis);
                extraLines = breakLinesCounter;
            }
            this.doc.setFont('helvetica', 'normal');
            if(documentItem.value.length < 55){
                this.doc.text(documentItem.value, hAxis + 70, yAxis);
            } else {
                const { outputString, breakLinesCounter } = this.splitBigString(documentItem.value, 55);
                this.doc.text(`${outputString}`, hAxis + 70, yAxis);
                extraLines = extraLines < breakLinesCounter ? breakLinesCounter : extraLines;
            }
            yAxis = yAxis + extraLines * 5 + 10;
            this.doc.line(hAxis, yAxis - 7, hAxis + 180, yAxis - 7);
        }
        this.doc.save(`process_${processId}.pdf`);
    }

    splitBigString(inputString: string, lineLimit: number) {
        const words = inputString.split(' ');
        let counter = 0;
        let breakLinesCounter = 0;
        let outputString = '';
        for(let i=0; i<words.length; i++){
            if(counter + words[i].length < lineLimit){
                outputString = outputString + words[i];
                counter = counter + words[i].length;
            }else{
                outputString = outputString.slice(0, outputString.length - 1);
                outputString = outputString + '\n' + words[i];
                counter = 0;
                breakLinesCounter++;
            }
            if(i !== words.length - 1){
                outputString = outputString + ' ';
                counter++;
            }  
        }
        return { outputString, breakLinesCounter };   
    }

    drawHeader() {
        this.doc.addImage(this.logo, "PNG", 10, 10, 40, 32);
        this.doc.setFont('courier');
        this.doc.setFontSize(34);
        this.doc.text('BLOCKCHAIN', 120, 25, {align: 'center'});
        this.doc.setFontSize(17);
        this.doc.text('BUSINESS PROCESS MANAGEMENT SYSTEM', 120, 35, {align: 'center'});
    }

    drawPageNumber(pageNumber: number) {
        this.doc.setFont('arial');
        this.doc.setFontSize(15);
        this.doc.text('[   ]', 105, 285, {align: 'center'});
        this.doc.setFontSize(14);
        this.doc.text(`${pageNumber}`, 105, 285, {align: 'center'});
    }
}
