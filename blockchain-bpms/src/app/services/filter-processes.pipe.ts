import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'filterProcesses'
})

export class FilterProcessesPipe implements PipeTransform {
    transform(items: any[], searchValue: string): any[] {
        if (!items) return [];
        if (!searchValue) return items;
    
        return items.filter((entry) => {
            return searchValue.indexOf(entry.processId) !== -1 || entry.processName.toLowerCase().includes(searchValue.toLowerCase());
        }, searchValue);

    }
}