import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'filterUsers'
})

export class FilterUsersPipe implements PipeTransform {
    transform(items: any[], searchValue: string): any[] {
        if (!items) return [];
        if (!searchValue) return items;
    
        return items.filter((entry) => {
            return searchValue.indexOf(entry.id) !== -1
                || entry.name.toLowerCase().includes(searchValue.toLowerCase())
                || entry.surname.toLowerCase().includes(searchValue.toLocaleLowerCase());
        }, searchValue);

    }
}