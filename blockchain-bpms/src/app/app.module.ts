import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { ClipboardModule } from '@angular/cdk/clipboard';
import { GoogleChartsModule } from 'angular-google-charts';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { EthcontractService } from './services/ethcontract.service';
import { DataService } from './services/data.service';
import { FilterUsersPipe } from './services/filter-users.pipe';
import { FilterProcessesPipe } from './services/filter-processes.pipe';

import { AppComponent } from './app.component';
import { NavbarComponent } from './components/core/navbar/navbar.component';
import { FooterComponent } from './components/core/footer/footer.component';
import { HomeComponent } from './components/home/home.component';
import { ProfileComponent } from './components/profile/profile.component';
import { UsersComponent } from './components/users/users.component';
import { UsersTableComponent } from './components/users/users-table/users-table.component';
import { AddUserComponent } from './components/users/add-user/add-user.component';
import { UserProfileComponent } from './components/users/user-profile/user-profile.component';
import { ProcessesComponent } from './components/processes/processes.component';
import { ProcessesTableComponent } from './components/processes/processes-table/processes-table.component';
import { AddProcessModalComponent } from './components/processes/add-process-modal/add-process-modal.component';
import { AddTaskModalComponent } from './components/processes/add-task-modal/add-task-modal.component';
import { TaskDetailsModalComponent } from './components/processes/task-details-modal/task-details-modal.component';
import { ConfirmationModalComponent } from './components/core/confirmation-modal/confirmation-modal.component';
import { ProcessDetailsComponent } from './components/processes/process-details/process-details.component';
import { CalendarChartComponent } from './components/core/calendar-chart/calendar-chart.component';
import { MetamaskComponent } from './components/core/metamask/metamask.component';
import { NotFoundComponent } from './components/core/not-found/not-found.component';


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    FooterComponent,
    HomeComponent,
    UsersComponent,
    UsersTableComponent,
    AddUserComponent,
    UserProfileComponent,
    ProcessesComponent,
    ProcessesTableComponent,
    AddProcessModalComponent,
    AddTaskModalComponent,
    TaskDetailsModalComponent,
    ConfirmationModalComponent,
    ProcessDetailsComponent,
    CalendarChartComponent,
    MetamaskComponent,
    NotFoundComponent,
    ProfileComponent,
    FilterUsersPipe,
    FilterProcessesPipe
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    AppRoutingModule,
    ReactiveFormsModule,
    NgbModule,
    BrowserAnimationsModule,
    MatSnackBarModule,
    MatFormFieldModule,
    MatInputModule,
    MatTooltipModule,
    MatCheckboxModule,
    MatProgressBarModule,
    GoogleChartsModule,
    ClipboardModule
  ],
  providers: [EthcontractService, DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
