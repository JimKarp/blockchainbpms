export class User {
    id: number;
    walletAddress: string;
    balance: number;
    name: string;
    surname: string;
    department: string;
    tasksCompleted: number;
    tasksPending: number;
    processesApproved: number;
    processesRejected: number;
    role: string;
    contributions: string[];
}