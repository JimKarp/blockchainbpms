import { Task } from './task';
import { User } from './user';

export class Process {
    processId: number;
    processName: string;
    generatedBy: number;
    generatedByEmployee: User;
    timestampGenerated: number;
    formattedTimestampGenerated: string;
    endedBy: number;
    endedByEmployee: User;
    timestampEnded: number;
    formattedTimestampEnded: string;
    status: string;
    checkedBy: number;
    checkedBySupervisor: User;
    timestampChecked: number;
    formattedTimestampChecked: string;
    tasksCounter: number;
    tasks: Task[];
    document: string;
    parsedDocument: any[];
}