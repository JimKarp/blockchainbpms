export enum Role {
    Employee = 'Employee',
    Supervisor = 'Supervisor',
    Director = 'Director',
    Admin = 'Admin'
}