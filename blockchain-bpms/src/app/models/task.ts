import { User } from './user';

export class Task {
    taskId: number;
    taskName: string;
    generatedBy: number;
    generatedByEmployee: User;
    timestampGenerated: number;
    formattedTimestampGenerated: string;
    acceptedBy: number;
    acceptedByEmployee: User;
    timestampAccepted: number;
    formattedTimestampAccepted: string;
    completedBy: number;
    completedByEmployee: User;
    timestampCompleted: number;
    formattedTimestampCompleted: string;
    duration: string;
    status: string;
    processId: number;
    processName: string;
}