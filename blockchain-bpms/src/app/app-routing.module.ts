import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from './components/home/home.component';
import { ProfileComponent } from './components/profile/profile.component';
import { UsersComponent } from './components/users/users.component';
import { AddUserComponent } from './components/users/add-user/add-user.component';
import { UserProfileComponent } from './components/users/user-profile/user-profile.component';
import { ProcessDetailsComponent } from './components/processes/process-details/process-details.component';
import { MetamaskComponent } from './components/core/metamask/metamask.component';
import { NotFoundComponent } from './components/core/not-found/not-found.component';
import { Role } from './models/role';
import { AuthGuard } from './services/auth.guard';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'employees',
    component: UsersComponent,
    canActivate: [AuthGuard],
    data: { roles: [Role.Supervisor, Role.Director, Role.Admin] }
  },
  {
    path: 'supervisors',
    component: UsersComponent,
    canActivate: [AuthGuard],
    data: { roles: [Role.Director, Role.Admin] }
  },
  {
    path: 'directors',
    component: UsersComponent,
    canActivate: [AuthGuard],
    data: { roles: [Role.Admin] }
  },
  {
    path: 'employees/add',
    component: AddUserComponent,
    canActivate: [AuthGuard],
    data: { roles: [Role.Supervisor, Role.Director, Role.Admin] }
  },
  {
    path: 'supervisors/add',
    component: AddUserComponent,
    canActivate: [AuthGuard],
    data: { roles: [Role.Director, Role.Admin] }
  },
  {
    path: 'directors/add',
    component: AddUserComponent,
    canActivate: [AuthGuard],
    data: { roles: [Role.Admin] }
  },
  {
    path: 'employees/:id',
    component: UserProfileComponent,
    canActivate: [AuthGuard],
    data: { roles: [Role.Supervisor, Role.Director, Role.Admin] }
  },
  {
    path: 'supervisors/:id',
    component: UserProfileComponent,
    canActivate: [AuthGuard],
    data: { roles: [Role.Director, Role.Admin] }
  },
  {
    path: 'directors/:id',
    component: UserProfileComponent,
    canActivate: [AuthGuard],
    data: { roles: [Role.Admin] }
  },
  {
    path: 'processes/:id',
    component: ProcessDetailsComponent,
    canActivate: [AuthGuard],
    data: { roles: [Role.Employee, Role.Supervisor, Role.Director, Role.Admin] }
  },
  {
    path: 'profile',
    component: ProfileComponent,
    canActivate: [AuthGuard],
    data: { roles: [Role.Employee, Role.Supervisor, Role.Director, Role.Admin] }
  },
  {
    path: 'metamask',
    component: MetamaskComponent
  },
  {path: '**', component: NotFoundComponent}
];

@NgModule({
  exports: [ RouterModule ],
  imports: [
    RouterModule.forRoot(routes)
  ]
})
export class AppRoutingModule { }