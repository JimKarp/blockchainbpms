module.exports = {
  networks : {
    ganache : {
      host : 'localhost',
      port : 8545,
      network_id : "*"
    },
    rpc : {
      host : '10.100.59.164',
      port : 8545,
      network_id : "15",
      from : '0xed0091b997c819067f420c950dbda8044e633b41',
      gas : 10000000
    }
  },
  compilers: {
    solc: {
      version: '0.5.16',
      optimizer: {
        enabled: true,
        runs: 200
      }
    }
  }
};